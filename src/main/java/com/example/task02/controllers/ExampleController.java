package com.example.task02.controllers;

import com.example.task02.model.Recordd;
import com.example.task02.model.RecordsDAO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
@Slf4j
public class ExampleController {
    private final RecordsDAO recordsDAO;

    @GetMapping("/")
    String main(Model model) {
        model.addAttribute("title", "model Attr");
        model.addAttribute("students", recordsDAO.getAll());
        return "main";
    }

    @GetMapping("/addcontact")
    String addcontact(Model model) {
        return "addcontact";
    }

    @GetMapping("/searchbyname")
    String searchbyname(Model model) {
        return "searchbyname";
    }

    @GetMapping("/deletethecontact")
    String deletethecontact(Model model) {
        return "deletethecontact";
    }

    @GetMapping("/students/edit/{id}")
    String mains(@PathVariable("id") String id,
                 @RequestHeader("Referer") String ref) {
        ModelAndView modelAndView = new ModelAndView("main");
        recordsDAO.deleteRecord(Integer.parseInt(id));
        modelAndView.addObject("students", recordsDAO.getAll());
        return "redirect:" + ref;
    }

    @PostMapping("/example")
    String test(
            @RequestParam("name") String name,
            @RequestParam("phone") String phone,
            @RequestParam("email") String email,
            @RequestHeader("Referer") String ref) {
        if (name == null || name.isEmpty() || phone == null || phone.isEmpty()) {
            throw new RuntimeException("Invalid param or surname");
        }
        recordsDAO.add(new Recordd(null, name, phone, email));
        return "redirect:" + ref;
    }

    @PostMapping("/result")
    String searchByName(
            @RequestParam("param") String param,
            @RequestParam("contact") String contact,
            @RequestHeader("Referer") String ref,
            Model model) {

        switch (contact) {
            case "name" -> {
                model.addAttribute("students", recordsDAO.getByName(param));
            }
            case "email" -> {
                model.addAttribute("students", recordsDAO.getByEmail(param));
            }
            case "phone" -> {
                model.addAttribute("students", recordsDAO.getByPhone(param));
            }
        }
        return "result";
    }

    @PostMapping("/deletethecontact")
    String deletethecontact(
            @RequestParam("id") String id,
            @RequestHeader("Referer") String ref) {
        recordsDAO.deleteRecord(Integer.parseInt(id));
        return "redirect:" + ref;
    }

    @PostMapping("/resultphone")
    String searchByPhone(
            @RequestParam("phone") String phone,
            @RequestHeader("Referer") String ref,
            Model model) {
        model.addAttribute("students", recordsDAO.getByPhone(phone));//.stream()
        return "resultphone";
    }
}
