package com.example.task02;

import com.example.task02.model.RecordsDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration
public class MyConfig {

    @Bean
    RecordsDAO recordsDAOEE(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        return new RecordsDAO(namedParameterJdbcTemplate);
    }
}
