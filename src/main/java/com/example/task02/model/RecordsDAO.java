package com.example.task02.model;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@RequiredArgsConstructor
public class RecordsDAO {
    private static final String GET_ALL_RECORDS = "select id, name, phone, email from recs";
    private static final String INSERT = "insert into recs(name, phone, email) values(:name, :phone, :email)";
    private static final String DELETE_RECORD = "delete from recs where id=:id";
    private static final String GET_BY_PHONE = "select id, name, phone, email from recs where phone=:phone";
    private static final String GET_BY_NAME = "select id, name, phone, email from recs where name=:name";
    private static final String GET_BY_EMAIL = "select id, name, phone, email from recs where email like concat(:email, '%')";
    private final NamedParameterJdbcTemplate template;
    private final static RowMapper<Recordd> RECORDD_ROW_MAPPER =
            (rs, i) -> Recordd.builder()
                    .id(rs.getInt("id"))
                    .name(rs.getString("name"))
                    .phone(rs.getString("phone"))
                    .email(rs.getString("email"))
                    .build();


    public List<Recordd> getAll() {
        return template.query(GET_ALL_RECORDS, RECORDD_ROW_MAPPER);
    }

    private Recordd mapRow(ResultSet resultSet) throws SQLException {
        return new Recordd(
                resultSet.getObject("id", Integer.class),
                resultSet.getString("name"),
                resultSet.getString("phone"),
                resultSet.getString("email")
        );
    }

    public void add(Recordd recordd) {
        template.update(INSERT, new MapSqlParameterSource()
                .addValue("name", recordd.getName())
                .addValue("phone", recordd.getPhone())
                .addValue("email", recordd.getEmail()));
    }

    public void deleteRecord(int id) {
        template.update(DELETE_RECORD,
                new MapSqlParameterSource().addValue(
                        "id", id
                ));
    }

    public List<Recordd> getByPhone(String phone) {
        return template.query(
                GET_BY_PHONE,
                new MapSqlParameterSource().addValue(
                        "phone", phone), RECORDD_ROW_MAPPER
        );
    }

    public List<Recordd> getByName(String name) {
        return template.query(
                GET_BY_NAME,
                new MapSqlParameterSource().addValue(
                        "name", name), RECORDD_ROW_MAPPER);
    }

    public List<Recordd> getByEmail(String email) {
        return template.query(
                GET_BY_EMAIL,
                new MapSqlParameterSource().addValue(
                        "email", email), RECORDD_ROW_MAPPER
        );
    }
}
