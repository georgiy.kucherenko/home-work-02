package com.example.task02.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class Recordd {
    Integer id;
    String name;
    String phone;
    String email;
}
