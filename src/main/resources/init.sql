--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Debian 15.1-1.pgdg110+1)
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: example; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE example WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';


ALTER DATABASE example OWNER TO postgres;

\connect example

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: recs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recs (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    phone character varying(13) NOT NULL,
    email character varying(50)
);


ALTER TABLE public.recs OWNER TO postgres;

--
-- Name: recs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recs_id_seq OWNER TO postgres;

--
-- Name: recs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recs_id_seq OWNED BY public.recs.id;


--
-- Name: recs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recs ALTER COLUMN id SET DEFAULT nextval('public.recs_id_seq'::regclass);


--
-- Data for Name: recs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.recs (id, name, phone, email) VALUES (2, 'Vasiliy', '+380675600561', 'ya@gmail.com');
INSERT INTO public.recs (id, name, phone, email) VALUES (7, 'Ruslan', '+380675600560', 'rumus@gmail.com');
INSERT INTO public.recs (id, name, phone, email) VALUES (29, 'ivan', '+380675600879', 'jk@gmail.com');
INSERT INTO public.recs (id, name, phone, email) VALUES (30, 'sasha', '+380637582077', 'sasha@i.ua');
INSERT INTO public.recs (id, name, phone, email) VALUES (31, 'igor', '+380676103030', 'igor@gmail.com');
INSERT INTO public.recs (id, name, phone, email) VALUES (32, 'sveta', '+380992770648', 'svetlana@ua.fm');


--
-- Name: recs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recs_id_seq', 32, true);


--
-- PostgreSQL database dump complete
--

